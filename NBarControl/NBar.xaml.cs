﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls.Primitives;

namespace NBarControl
{
    /// <summary>
    /// Interaction logic for NBar.xaml
    /// </summary>
    public partial class NBar : UserControl
    {
        # region Class constructors

        public NBar()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var indent = new Thickness(10, 2, 10, 2);
            var margin = new Thickness(0, 0, 0, 0);
            PreInitRectangles(_numberOfBands, this.Width, this.Height, margin, indent);

            Ready?.Invoke(this, EventArgs.Empty);
        }

        # endregion

        # region Extra Structures

        private struct RecState
        {
            public double _width;
            public double _height;
            public Thickness _thickness;

            public RecState(double width, double height, Thickness thickness)
            {
                _width = width;
                _height = height;
                _thickness = thickness;
            }
        }

        private struct RecText
        {
            public int _start;
            public int _end;

            public RecText(int start, int end)
            {
                _start = start;
                _end = end;
            }
        }

        List<RecText> recTexts = new List<RecText>();

        # endregion

        # region Private Variables

        private Popup popup = new Popup();

        private List<RecState> StandartList = new List<RecState>();

        private double _widthOne;

        private int _currentIndex = -1;

        private int _previosIndex = -1;

        private bool _isLeave = true;

        # endregion

        # region Private Saved Variables
        private Point savedActiveRectangle = new Point(-1, -1);

        private int[] savedRPRecIndexes;

        private int[] savedRSRecIndexes;

        private int[] savedActiveRSCircles;

        private int[] savedNonActiveRSCircles;

        # endregion


        private int _Mode = 0;
        public int Mode
        {
            get { return _Mode; }
            set
            {
                if (_Mode != value)
                {
                    _Mode = value;
                }
            }
        }

        # region Properties

        private int _numberOfBands;
        public int NumberOfBands
        {
            get { return _numberOfBands; }
            set
            {
                if (_numberOfBands != value)
                {
                    _numberOfBands = value;
                    ReInit(_numberOfBands, _leftMargin, _rightMargin, _topMargin, _bottomMargin);
                }
            }
        }

        private double _rangeMinMHz = 25.0;
        public double RangeMinMHz
        {
            get { return _rangeMinMHz; }
            set
            {
                if (_rangeMinMHz != value)
                    _rangeMinMHz = value;
            }
        }

        private double _bandWidthMHz = 30.0;
        public double BandWidthMHz
        {
            get { return _bandWidthMHz; }
            set
            {
                if (_bandWidthMHz != value)
                    _bandWidthMHz = value;
            }
        }

        private double _overlayMHz = 0.0;
        public double OverlayMHz
        {
            get { return _overlayMHz; }
            set
            {
                if (_overlayMHz != value)
                    _overlayMHz = value;
            }
        }

        private int _numberOfViewBands = 5;
        public int NumberOfViewBands
        {
            get { return _numberOfViewBands; }
            set
            {
                if (_numberOfViewBands != value)
                    _numberOfViewBands = value;
            }
        }

        //private Color _activeRecColor = Colors.Red;
        private Color _activeRecColor = Color.FromRgb(255, 140, 0);
        //private Color _activeRecColor = Color.FromRgb(160, 82, 45);
        //private Color _activeRecColor = Color.FromRgb(128, 64, 0);
        public Color ActiveRecColor
        {
            get { return _activeRecColor; }
            set
            {
                if (_activeRecColor != value)
                {
                    _activeRecColor = value;
                    RepaintActiveRec(_activeRecColor);
                }
            }
        }

        private Color _rpRecsColor = Colors.Blue;
        public Color RPRecsColor
        {
            get { return _rpRecsColor; }
            set
            {
                if (_rpRecsColor != value)
                {
                    _rpRecsColor = value;
                    RepaintRecs(_rpRecsColor);
                }
            }
        }

        private Color _rsRecsColor = Colors.Aqua;
        public Color RSRecsColor
        {
            get { return _rsRecsColor; }
            set
            {
                if (_rsRecsColor != value)
                {
                    _rsRecsColor = value;
                    RepaintRecs(_rsRecsColor);
                }
            }
        }

        private Color _rsEmitActiveColor = Colors.Red;
        public Color RSEmitActiveColor
        {
            get { return _rsEmitActiveColor; }
            set
            {
                if (_rsEmitActiveColor != value)
                {
                    RepaintCircles(_rsEmitActiveColor, value);
                    _rsEmitActiveColor = value;
                }
            }
        }

        private Color _rsEmitNonActiveColor = Colors.Pink;
        public Color RSEmitNonActiveColor
        {
            get { return _rsEmitNonActiveColor; }
            set
            {
                if (_rsEmitNonActiveColor != value)
                {
                    RepaintCircles(_rsEmitNonActiveColor, value);
                    _rsEmitNonActiveColor = value;
                }
            }
        }

        //private Color _BackGround = Colors.Transparent;
        private Color _BackGround = Color.FromRgb(51, 51, 51);
        public Color BackGround
        {
            get { return _BackGround; }
            set
            {
                if (_BackGround != value)
                {
                    _BackGround = value;
                    canvas.Background = new SolidColorBrush(value);
                }
            }
        }

        //private Color _RectanglesColor = Colors.Black;
        private Color _RectanglesColor = Color.FromRgb(204, 204, 204);
        public Color RectanglesColor
        {
            get { return _RectanglesColor; }
            set
            {
                if (_RectanglesColor != value)
                {
                    _RectanglesColor = value;

                    for (int i = 0; i < canvas.Children.Count; i++)
                    {
                        if (canvas.Children[i] is Rectangle)
                        {
                            var r = (Rectangle)canvas.Children[i];
                            r.Stroke = new SolidColorBrush(_RectanglesColor);
                        }
                    }
                }
            }
        }

        private Color _PopUpColor = Colors.White;
        public Color PopUpColor
        {
            get { return _PopUpColor; }
            set
            {
                if (_PopUpColor != value)
                {
                    _PopUpColor = value;
                    if (popup.IsOpen)
                    {
                        var tb = (TextBlock)popup.Child;
                        tb.Foreground = new SolidColorBrush(value);
                        popup.Child = tb;
                    }
                }
            }
        }

        # endregion

        #region Private Properties

        private double _leftMargin;
        private double LeftMargin
        {
            get { return _leftMargin; }
            set
            {
                _leftMargin = value;
                ReInit(_numberOfBands, _leftMargin, _rightMargin, _topMargin, _bottomMargin);
            }
        }

        private double _rightMargin;
        private double RightMargin
        {
            get { return _rightMargin; }
            set
            {
                _rightMargin = value;
                ReInit(_numberOfBands, _leftMargin, _rightMargin, _topMargin, _bottomMargin);
            }
        }

        private double _topMargin;
        private double TopMargin
        {
            get { return _topMargin; }
            set
            {
                _topMargin = value;
                ReInit(_numberOfBands, _leftMargin, _rightMargin, _topMargin, _bottomMargin);
            }
        }

        private double _bottomMargin;
        private double BottomMargin
        {
            get { return _bottomMargin; }
            set
            {
                _bottomMargin = value;
                ReInit(_numberOfBands, _leftMargin, _rightMargin, _topMargin, _bottomMargin);
            }
        }

        private double _width;
        private double cWidth
        {
            get { return _width; }
            set
            {
                _width = value;
                InitCanvas(_width, _height, _margin);
            }
        }

        private double _height;
        private double cHeight
        {
            get { return _height; }
            set
            {
                _height = value;
                InitCanvas(_width, _height, _margin);
            }
        }

        private Thickness _margin;
        private Thickness cMargin
        {
            get { return _margin; }
            set
            {
                _margin = value;
                InitCanvas(_width, _height, _margin);
            }
        }

        #endregion

        # region Public Metods

        public void DrawActiveRectangle(int startIndex, int endIndex)
        {
            int min = Math.Min(startIndex, endIndex);
            int max = Math.Max(startIndex, endIndex);

            if (min < 0) min = 0;
            if (max > _numberOfBands - 1) max = _numberOfBands - 1;

            if (min > _numberOfBands - 1) return;

            startIndex = min;
            endIndex = max;

            DrawActiveRec(startIndex, endIndex, _activeRecColor);
            savedActiveRectangle = new Point(startIndex, endIndex);
        }

        public void FillRPRectangles(int[] indexes)
        {
            if (indexes != null)
            {
                indexes = indexes.Distinct().ToArray();

                SetRecColor(indexes, _rpRecsColor);
                savedRPRecIndexes = indexes;
            }
        }

        public void FillRSRectangles(int[] indexes)
        {
            if (indexes != null)
            {
                indexes = indexes.Distinct().ToArray();

                SetRecColor(indexes, _rsRecsColor);
                savedRSRecIndexes = indexes;
            }
        }

        public void ClearRectangles()
        {
            for (int i = 0; i < _numberOfBands; i++)
            {
                var temp = (Rectangle)canvas.Children[i];
                temp.Fill = new SolidColorBrush(Colors.Transparent);
                canvas.Children[i] = temp;
            }
        }

        public void DrawActiveRSCircles(int[] indexes)
        {
            if (indexes != null)
            {
                indexes = indexes.Distinct().ToArray();

                DrawCircle(indexes, _rsEmitActiveColor);
                savedActiveRSCircles = indexes;
            }
        }

        public void DrawNonActiveRSCircles(int[] indexes)
        {
            if (indexes != null)
            {
                indexes = indexes.Distinct().ToArray();

                DrawCircle(indexes, _rsEmitNonActiveColor);
                savedNonActiveRSCircles = indexes;
            }
        }

        public void ClearRSCircles()
        {
            ClearCircles();
            savedActiveRSCircles = null;
            savedNonActiveRSCircles = null;
        }

        # endregion

        #region Classes 4 Events

        public class SimpleIntEventArgs
        {
            public int Index;
            public SimpleIntEventArgs(int index)
            {
                Index = index;
            }
        }

        public class DoubleIntEventArgs
        {
            public int StartIndex;
            public int EndIndex;
            public DoubleIntEventArgs(int startIndex, int endIndex)
            {
                StartIndex = startIndex;
                EndIndex = endIndex;
            }
        }

        public class RangeIntEventArgs
        {
            public int StartIntRange;
            public int EndIntRange;
            public RangeIntEventArgs(int startRange, int endRange)
            {
                StartIntRange = startRange;
                EndIntRange = endRange;
            }
        }

        #endregion

        # region Public Events
        public delegate void isReady(object sender, EventArgs e);
        public event isReady Ready;

        public delegate void ClickEH(object sender, SimpleIntEventArgs e);
        public event ClickEH ClickOnLeft;

        public delegate void ClickRangeInt(object sender, RangeIntEventArgs e);
        public event ClickRangeInt ClickOnLeftRange;

        public delegate void ClickEH2(object sender, DoubleIntEventArgs e);
        public event ClickEH2 ClickOnRight;

        public event ClickEH ClickOnMiddle;

        # endregion

        # region Private Metods

        private void InitCanvas(double canvasWidth, double canvasHeight, Thickness canvasMargin)
        {
            canvas.Width = canvasWidth;
            canvas.Height = canvasHeight;
            canvas.Margin = canvasMargin;
        }

        private void PreInitRectangles(int NumberOfBands, double Width, double Height, Thickness thickness, Thickness indent)
        {
            if (NumberOfBands == 0) NumberOfBands = 100;
            InitCanvas(Width, Height, thickness);
            InitRectangles(NumberOfBands, indent.Left, indent.Right, indent.Top, indent.Bottom);
        }

        private void InitRectangles(int NumberOfBands, double LeftMargin, double RightMargin, double TopMargin, double BottomMargin)
        {
            _numberOfBands = NumberOfBands;
            _leftMargin = LeftMargin;
            _rightMargin = RightMargin;
            _topMargin = TopMargin;
            _bottomMargin = BottomMargin;
            canvas.Children.Clear();
            StandartList.Clear();
            for (int i = 0; i < NumberOfBands; i++)
            {
                Rectangle r = new Rectangle();
                try { r.Width = canvas.ActualWidth - (LeftMargin + RightMargin); }
                catch { r.Width = 0; }
                r.Width = (r.Width / NumberOfBands);
                _widthOne = r.Width;
                r.Height = canvas.ActualHeight - (TopMargin + BottomMargin);
                r.Margin = new Thickness(LeftMargin + r.Width * i, 2, 0, 0);
                StandartList.Add(new RecState(r.Width, r.Height, r.Margin));

                r.Fill = new SolidColorBrush(Colors.Transparent);
                r.Stroke = new SolidColorBrush(_RectanglesColor);
                r.StrokeThickness = 1;

                r.MouseEnter += r_MouseEnter;
                r.MouseLeave += r_MouseLeave;

                r.MouseDown += r_MouseDown;

                canvas.Children.Add(r);
            }

            FillRecTexts(NumberOfBands, RangeMinMHz, BandWidthMHz, OverlayMHz);
        }

        private void FillRecTexts(int numberOfBands, double rangeMinMHz, double bandWidthMHz, double overlayMHz)
        {
            double rangeMaxMHz = rangeMinMHz + numberOfBands * bandWidthMHz;

            recTexts.Clear();

            int i = 0;
            while (rangeMinMHz < rangeMaxMHz)
            {
                if (recTexts.Count == 0)
                {
                    recTexts.Add(new RecText((int)rangeMinMHz, (int)(rangeMinMHz + bandWidthMHz)));
                }
                else
                {
                    recTexts.Add(new RecText((int)(recTexts[i - 1]._end - overlayMHz), (int)(recTexts[i - 1]._end - overlayMHz + bandWidthMHz)));
                }
                i++;
                rangeMinMHz += bandWidthMHz - overlayMHz;
            }
        }

        private void ReInit(int NumberOfBands, double LeftMargin, double RightMargin, double TopMargin, double BottomMargin)
        {
            StandartList.Clear();
            canvas.Children.Clear();
            var indent = new Thickness(10, 2, 10, 2);
            InitRectangles(NumberOfBands, LeftMargin, RightMargin, TopMargin, BottomMargin);
        }

        # endregion

        # region Internal Metods

        private void DrawActiveRec(int start, int end, Color color)
        {
            Polyline ActiveRec = new Polyline();
            ActiveRec.Points = new PointCollection();

            ActiveRec.StrokeThickness = 2;
            ActiveRec.Stroke = new SolidColorBrush(_activeRecColor);

            for (int i = start; i <= end; i++)
            {
                var temp = (Rectangle)canvas.Children[i];

                ActiveRec.Points.Add(new Point(temp.Margin.Left, temp.Margin.Top));
                ActiveRec.Points.Add(new Point(temp.Margin.Left + temp.Width, temp.Margin.Top));
            }

            for (int i = end; (i >= start) && (i <= end); i--)
            {
                var temp = (Rectangle)canvas.Children[i];

                ActiveRec.Points.Add(new Point(temp.Margin.Left + temp.Width, temp.Margin.Top + temp.Height));
                ActiveRec.Points.Add(new Point(temp.Margin.Left, temp.Margin.Top + temp.Height));
            }

            ActiveRec.Points.Add(ActiveRec.Points[0]);

            if (canvas.Children.Count < NumberOfBands + 1)
            {
                canvas.Children.Add(ActiveRec);
            }
            else
            {
                canvas.Children.RemoveAt(NumberOfBands);
                canvas.Children.Insert(NumberOfBands, ActiveRec);
            }
        }

        private void RepaintActiveRec(Color color)
        {
            if (canvas.Children.Count > 0 && _numberOfBands > 0)
            {
                var activeRec = (Polyline)canvas.Children[_numberOfBands];
                activeRec.Stroke = new SolidColorBrush(color);
            }
        }

        private void SetRecColor(int[] indexOf, Color color)
        {
            for (int i = 0; i < _numberOfBands; i++)
            {
                var temp = (Rectangle)canvas.Children[i];
                if (indexOf.Contains(i))
                {
                    temp.Fill = new SolidColorBrush(color);
                    canvas.Children[i] = temp;
                }
                else
                {
                    temp.Fill = new SolidColorBrush(Colors.Transparent);
                    canvas.Children[i] = temp;
                }
            }
        }

        private void RepaintRecs(Color color)
        {
            for (int i = 0; i < _numberOfBands; i++)
            {
                var temp = (Rectangle)canvas.Children[i];
                var brush = temp.Fill;
                if (brush.ToString() != "#00FFFFFF")
                {
                    temp.Fill = new SolidColorBrush(color);
                    canvas.Children[i] = temp;
                }
            }
        }

        private void DrawCircle(int[] indexOf, Color color)
        {
            for (int i = 0; i < canvas.Children.Count; i++)
            {
                if (canvas.Children[i] is Ellipse)
                {
                    var temp = (Ellipse)canvas.Children[i];
                    var brush = temp.Fill;
                    if (brush.ToString() == color.ToString())
                    {
                        canvas.Children.RemoveAt(i);
                        i--;
                    }
                }
            }

            for (int i = 0; i < _numberOfBands; i++)
            {
                if (indexOf.Contains(i))
                {
                    var temp = (Rectangle)canvas.Children[i];
                    Ellipse el = new Ellipse();
                    el.Width = el.Height = Math.Min(temp.Width, temp.Height);

                    el.Margin = new Thickness(temp.Margin.Left, (canvas.ActualHeight - 2 - 2 - el.Height) / 2, 0, 0);
                    //Console.WriteLine(el.Margin);
                    el.Fill = new SolidColorBrush(color);

                    el.MouseEnter += r_MouseEnter;
                    el.MouseLeave += r_MouseLeave;

                    el.MouseDown += r_MouseDown;

                    canvas.Children.Add(el);
                }
            }
        }

        private void RepaintCircles(Color oldColor, Color newColor)
        {
            for (int i = 0; i < canvas.Children.Count; i++)
            {
                if (canvas.Children[i] is Ellipse)
                {
                    var temp = (Ellipse)canvas.Children[i];
                    var brush = temp.Fill;
                    if (brush.ToString() == oldColor.ToString())
                    {
                        temp.Fill = new SolidColorBrush(newColor);
                        canvas.Children[i] = temp;
                    }
                }
            }
        }

        private void ClearCircles()
        {
            if (canvas.Children.Count > _numberOfBands)
            {
                canvas.Children.RemoveRange(_numberOfBands + 1, canvas.Children.Count - _numberOfBands - 1);
            }
            if (canvas.Children[_numberOfBands] is Ellipse)
            {
                canvas.Children.RemoveAt(_numberOfBands);
            }
        }
        # endregion

        # region Internal Events

        private void r_MouseEnter(object sender, MouseEventArgs e)
        {
            if (_currentIndex == -1)
            {
                _currentIndex = GetIndexFromMouse(e);
            }
            else
            {
                if (_currentIndex != GetIndexFromMouse(e))
                {
                    _currentIndex = GetIndexFromMouse(e);
                    Standart();
                    UpdateActiveRec();
                    UpdateCircles();

                    popup.IsOpen = false;

                    _isLeave = true;
                }
            }

            if (_isLeave == true)
            {
                int index = GetIndexFromMouse(e);
                _previosIndex = index;
                if (index == 0)
                {
                    FirstIndex(index);
                    UpdateActiveRec();
                    UpdateCircles();
                }
                if (index > 0 && index < _numberOfBands - 1)
                {
                    AnyIndex(index);
                    UpdateActiveRec();
                    UpdateCircles();
                }
                if (index == _numberOfBands - 1)
                {
                    LastIndex(index);
                    UpdateActiveRec();
                    UpdateCircles();
                }

                popup.Child = generatePopupChild(index);
                popup.PlacementTarget = canvas.Children[index];
                popup.HorizontalOffset = 0;
                popup.AllowsTransparency = true;
                if (canvas.ActualWidth - e.GetPosition(canvas).X < 55)
                {
                    popup.HorizontalOffset = -50 + _widthOne;
                }
                popup.VerticalOffset = -canvas.ActualHeight - 15;
                popup.IsOpen = true;
                _isLeave = false;
            }
        }

        private TextBlock generatePopupChild(int index)
        {
            TextBlock tb = new TextBlock();
            //int start = (int)(_rangeMinMHz + index * _bandWidthMHz);
            //int end = (int)(start + _bandWidthMHz);
            int start = recTexts[index]._start;
            int end = recTexts[index]._end;
            tb.Text = start.ToString() + "-" + end.ToString();
            tb.Foreground = new SolidColorBrush(_PopUpColor);
            return tb;
        }

        private void UpdateActiveRec()
        {
            if (savedActiveRectangle.X != -1 && savedActiveRectangle.Y != -1)
            {
                var activeRec = (Polyline)canvas.Children[_numberOfBands];

                var Points = new PointCollection();

                int start = (int)savedActiveRectangle.X;
                int end = (int)savedActiveRectangle.Y;

                for (int i = start; i <= end; i++)
                {
                    var temp = (Rectangle)canvas.Children[i];

                    Points.Add(new Point(temp.Margin.Left, temp.Margin.Top));
                    Points.Add(new Point(temp.Margin.Left + temp.Width, temp.Margin.Top));
                }

                for (int i = end; (i >= start) && (i <= end); i--)
                {
                    var temp = (Rectangle)canvas.Children[i];

                    Points.Add(new Point(temp.Margin.Left + temp.Width, temp.Margin.Top + temp.Height));
                    Points.Add(new Point(temp.Margin.Left, temp.Margin.Top + temp.Height));
                }

                Points.Add(Points[0]);

                activeRec.Points = Points;
            }
        }

        private void UpdateCircles()
        {
            List<int> lint = new List<int>();
            List<int> lint2 = new List<int>();
            for (int i = 0; i < canvas.Children.Count; i++)
            {
                if (canvas.Children[i] is Ellipse)
                {
                    var temp = (Ellipse)canvas.Children[i];
                    var brush = temp.Fill;
                    if (brush.ToString() == RSEmitActiveColor.ToString())
                    {
                        lint.Add(i);
                    }
                    if (brush.ToString() == RSEmitNonActiveColor.ToString())
                    {
                        lint2.Add(i);
                    }
                }
            }

            if (savedActiveRSCircles != null && savedActiveRSCircles.Count() > 0)
            {
                for (int i = 0; i < savedActiveRSCircles.Count(); i++)
                {
                    var tempRec = (Rectangle)canvas.Children[savedActiveRSCircles[i]];
                    var tempCircle = (Ellipse)canvas.Children[lint[i]];

                    tempCircle.Width = tempCircle.Height = Math.Min(tempRec.Width, tempRec.Height);
                    tempCircle.Margin = new Thickness(tempRec.Margin.Left, (canvas.ActualHeight - 2 - 2 - tempCircle.Height) / 2, 0, 0);

                    canvas.Children.RemoveAt(lint[i]);
                    canvas.Children.Insert(lint[i], tempCircle);
                }
            }

            if (savedNonActiveRSCircles != null && savedNonActiveRSCircles.Count() > 0)
            {
                for (int i = 0; i < savedNonActiveRSCircles.Count(); i++)
                {
                    var tempRec = (Rectangle)canvas.Children[savedNonActiveRSCircles[i]];
                    var tempCircle = (Ellipse)canvas.Children[lint2[i]];

                    tempCircle.Width = tempCircle.Height = Math.Min(tempRec.Width, tempRec.Height);
                    tempCircle.Margin = new Thickness(tempRec.Margin.Left, (canvas.ActualHeight - 2 - 2 - tempCircle.Height) / 2, 0, 0);

                    canvas.Children.RemoveAt(lint2[i]);
                    canvas.Children.Insert(lint2[i], tempCircle);
                }
            }

        }

        private void r_MouseLeave(object sender, MouseEventArgs e)
        {
            if (RealLeave(e))
            {
                Standart();
                UpdateActiveRec();
                UpdateCircles();

                _isLeave = true;
                _currentIndex = -1;

                popup.IsOpen = false;
            }
        }

        private void r_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                ClickOnLeft?.Invoke(this, new SimpleIntEventArgs(GetIndex4Mouse(e)));
                ClickOnLeftRange?.Invoke(this, new RangeIntEventArgs(recTexts[GetIndex4Mouse(e)]._start, recTexts[GetIndex4Mouse(e)]._end));
            }
            if (e.ChangedButton == MouseButton.Right)
            {
                if (ClickOnRight != null)
                {
                    var P = Rec5(GetIndex4Mouse(e));
                    ClickOnRight(this, new DoubleIntEventArgs((int)P.X, (int)P.Y));
                }
            }
            if (e.ChangedButton == MouseButton.Middle)
            {
                ClickOnMiddle?.Invoke(this, new SimpleIntEventArgs(GetIndex4Mouse(e)));
            }
        }

        # endregion

        # region Internal Metods 4 Internal Events

        private double ShiftX()
        {
            var minX = Math.Min(_leftMargin, _rightMargin);
            return minX / 2.0;
        }

        private double ShiftY()
        {
            var minY = Math.Min(_topMargin, _bottomMargin);
            return minY;
        }

        private void FirstIndex(int index)
        {
            var shiftX = ShiftX();
            var shiftY = ShiftY();

            var currRec = (Rectangle)canvas.Children[index];
            currRec.Margin = new Thickness(currRec.Margin.Left - shiftX, currRec.Margin.Top - shiftY, currRec.Margin.Right, currRec.Margin.Bottom);
            currRec.Width = shiftX + currRec.Width + shiftX;
            currRec.Height = shiftY + currRec.Height + shiftY;

            int nextindex = index + 1;
            var nextRec = (Rectangle)canvas.Children[nextindex];
            nextRec.Margin = new Thickness(nextRec.Margin.Left + shiftX, nextRec.Margin.Top - shiftY / 2.0, nextRec.Margin.Right, nextRec.Margin.Bottom);
            nextRec.Width = nextRec.Width + shiftX;
            nextRec.Height = nextRec.Height + shiftY;

            for (int i = 2; i < _numberOfBands; i++)
            {
                var curRec = (Rectangle)canvas.Children[i];
                curRec.Margin = new Thickness(curRec.Margin.Left + 2 * shiftX, curRec.Margin.Top, curRec.Margin.Right, curRec.Margin.Bottom);
            }
        }

        private void AnyIndex(int index)
        {
            var shiftX = ShiftX();
            var shiftY = ShiftY();

            var currRec = (Rectangle)canvas.Children[index];
            currRec.Margin = new Thickness(currRec.Margin.Left - shiftX, currRec.Margin.Top - shiftY, currRec.Margin.Right, currRec.Margin.Bottom);
            currRec.Width = shiftX + currRec.Width + shiftX;
            currRec.Height = shiftY + currRec.Height + shiftY;

            int previndex = index - 1;
            var prevRec = (Rectangle)canvas.Children[previndex];
            prevRec.Margin = new Thickness(prevRec.Margin.Left - 2 * shiftX, prevRec.Margin.Top - shiftY / 2.0, prevRec.Margin.Right, prevRec.Margin.Bottom);
            prevRec.Width = prevRec.Width + shiftX;
            prevRec.Height = prevRec.Height + shiftY;

            for (int i = 0; i < previndex; i++)
            {
                var curRec = (Rectangle)canvas.Children[i];
                curRec.Margin = new Thickness(curRec.Margin.Left - 2 * shiftX, curRec.Margin.Top, curRec.Margin.Right, curRec.Margin.Bottom);
            }

            int nextindex = index + 1;
            var nextRec = (Rectangle)canvas.Children[nextindex];
            nextRec.Margin = new Thickness(nextRec.Margin.Left + shiftX, nextRec.Margin.Top - shiftY / 2.0, nextRec.Margin.Right, nextRec.Margin.Bottom);
            nextRec.Width = nextRec.Width + shiftX;
            nextRec.Height = nextRec.Height + shiftY;

            for (int i = nextindex + 1; i < _numberOfBands; i++)
            {
                var curRec = (Rectangle)canvas.Children[i];
                curRec.Margin = new Thickness(curRec.Margin.Left + 2 * shiftX, curRec.Margin.Top, curRec.Margin.Right, curRec.Margin.Bottom);
            }

        }

        private void LastIndex(int index)
        {
            var shiftX = ShiftX();
            var shiftY = ShiftY();

            var currRec = (Rectangle)canvas.Children[index];
            currRec.Margin = new Thickness(currRec.Margin.Left - shiftX, currRec.Margin.Top - shiftY, currRec.Margin.Right, currRec.Margin.Bottom);
            currRec.Width = shiftX + currRec.Width + shiftX;
            currRec.Height = shiftY + currRec.Height + shiftY;

            int previndex = index - 1;
            var prevRec = (Rectangle)canvas.Children[previndex];
            prevRec.Margin = new Thickness(prevRec.Margin.Left - 2 * shiftX, prevRec.Margin.Top - shiftY / 2.0, prevRec.Margin.Right, prevRec.Margin.Bottom);
            prevRec.Width = prevRec.Width + shiftX;
            prevRec.Height = prevRec.Height + shiftY;

            for (int i = 0; i < previndex; i++)
            {
                var curRec = (Rectangle)canvas.Children[i];
                curRec.Margin = new Thickness(curRec.Margin.Left - 2 * shiftX, curRec.Margin.Top, curRec.Margin.Right, curRec.Margin.Bottom);
            }
        }

        private void Standart()
        {
            for (int i = 0; i < _numberOfBands; i++)
            {
                var currRec = (Rectangle)canvas.Children[i];
                currRec.Height = StandartList[i]._height;
                currRec.Width = StandartList[i]._width;
                currRec.Margin = StandartList[i]._thickness;
            }
        }

        private bool RealLeave(MouseEventArgs e)
        {
            int index = GetIndexFromMouse(e);
            var Rec = (Rectangle)canvas.Children[index];
            var Pos = e.GetPosition(canvas);

            if ((Rec.Margin.Left <= Pos.X) &&
                (Rec.Margin.Left <= Pos.X - 1.5) &&
                (Pos.X <= Rec.Margin.Left + Rec.ActualWidth) &&
                (Pos.X + 1.5 <= Rec.Margin.Left + Rec.ActualWidth) &&
                (Rec.Margin.Top <= Pos.Y) &&
                (Rec.Margin.Top <= Pos.Y - 1.5) &&
                (Pos.Y <= Rec.Margin.Top + Rec.ActualHeight) &&
                (Pos.Y + 1.5 <= Rec.Margin.Top + Rec.ActualHeight))
            {
                return false; // внутри по иксу и по игрику
            }
            return true;
        }


        private int GetIndex4Mouse(MouseEventArgs e)
        {
            var pos = e.GetPosition(canvas);
            var posX = pos.X;
            int index = GetRealIndex(posX);
            if (index < 0) index = 0;
            if (index >= _numberOfBands) index = _numberOfBands - 1;
            return index;
        }

        private int GetRealIndex(double posX)
        {
            for (int i = 0; i < canvas.Children.Count; i++)
            {
                if (canvas.Children[i] is Rectangle)
                {
                    var tempRec = (Rectangle)canvas.Children[i];

                    if (tempRec.Margin.Left < posX && posX < tempRec.Margin.Left + tempRec.Width)
                    {
                        return i;
                    }
                }
            }

            return 0;
        }

        private int GetIndexFromMouse(MouseEventArgs e)
        {
            var pos = e.GetPosition(canvas);
            var posX = pos.X;
            int index = GetIndex(posX);
            if (index < 0) index = 0;
            if (index >= _numberOfBands) index = _numberOfBands - 1;
            return index;
        }

        private int GetIndex(double posX)
        {
            double w = posX - _leftMargin;
            double i = w / _widthOne;
            int ind = (int)i;
            return ind;
        }

        private Point Rec5(int index)
        {
            int viewBandsCount = (_numberOfViewBands - 1) / 2;

            int startIndex = index - viewBandsCount;
            int endIndex = index + viewBandsCount;
            return new Point((startIndex < 0) ? 0 : startIndex, (endIndex >= _numberOfBands) ? _numberOfBands - 1 : endIndex);
        }

        # endregion

        private void canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ReInit(_numberOfBands, _leftMargin, _rightMargin, _topMargin, _bottomMargin);
            Restore();
        }

        private void Restore()
        {
            if (savedActiveRectangle != new Point(-1, -1))
            {
                DrawActiveRec((int)savedActiveRectangle.X, (int)savedActiveRectangle.Y, _activeRecColor);
            }
            if (_Mode == 0 || _Mode == 1 || _Mode == 2)
                if (savedRPRecIndexes != null)
                {
                    SetRecColor(savedRPRecIndexes, _rpRecsColor);
                }
            if (_Mode == 3 || _Mode == 4 || _Mode == 5 || _Mode == 6)
                if (savedRSRecIndexes != null)
                {
                    SetRecColor(savedRSRecIndexes, _rsRecsColor);
                }
            RestoreCircles();
        }

        private void RestoreCircles()
        {
            if (savedActiveRSCircles != null)
            {
                DrawCircle(savedActiveRSCircles, _rsEmitActiveColor);
            }
            if (savedNonActiveRSCircles != null)
            {
                DrawCircle(savedNonActiveRSCircles, _rsEmitNonActiveColor);
            }
        }
    }
}
