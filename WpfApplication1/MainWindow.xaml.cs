﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using NBarControl;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
           InitializeComponent();

            n.ClickOnLeft += N_ClickOnLeft;
            n.ClickOnRight += N_ClickOnRight;
            n.ClickOnMiddle += N_ClickOnMiddle;
        }

        private void N_ClickOnLeft(object sender, NBar.SimpleIntEventArgs e)
        {
            Console.Beep();
        }

        private void N_ClickOnRight(object sender, NBar.DoubleIntEventArgs e)
        {
            Console.Beep();
        }

        private void N_ClickOnMiddle(object sender, NBar.SimpleIntEventArgs e)
        {
            Console.Beep();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private bool flag1 = false;
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //s2.Children.Clear();
            //n = new NBar(20, 300, 46, new Thickness(10, 10, 10, 10), new Thickness(10, 2, 10, 2));
            //s2.Children.Add(n);

            n.DrawActiveRectangle(200, 300);
            
            flag1 = !flag1;
            if (flag1)
            {
                Random r = new Random();
                var r1 = r.Next(0, 99);
                var r2 = r.Next(0, 99);
                n.DrawActiveRectangle(Math.Min(r1, r2), Math.Max(r1, r2));
            }
            else
            {   Random r = new Random();
                n.ActiveRecColor = Color.FromRgb((byte)r.Next(0, 255), (byte)r.Next(0, 255), (byte)r.Next(0, 255));
            }
            

        }

        private bool flag2 = false;
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            //s1.Children.Clear();
            //n1 = new NBar(10, 500, 50, new Thickness(10, 10, 10, 10), new Thickness(20, 2, 20, 2));
            //s1.Children.Add(n1);

            flag2 = !flag2;
            if (flag2)
            {
                Random r = new Random();

                var N = r.Next(0, 99);

                int [] indexes = new int[N];

                for (int i = 0; i < N; i++)
                {
                    indexes[i] = r.Next(0, 99);
                }
                n.FillRPRectangles(indexes);
            }
            else
            {
                Random r = new Random();
                n.RPRecsColor = Color.FromRgb((byte)r.Next(0, 255), (byte)r.Next(0, 255), (byte)r.Next(0, 255));
            }

        }

        private bool flag3 = false;
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            flag3 = !flag3;
            if (flag3)
            {
                Random r = new Random();

                var N = r.Next(0, 99);

                int[] indexes = new int[N];

                for (int i = 0; i < N; i++)
                {
                    indexes[i] = r.Next(0, 99);
                }
                n.FillRSRectangles(indexes);
            }
            else
            {
                Random r = new Random();
                n.RSRecsColor = Color.FromRgb((byte)r.Next(0, 255), (byte)r.Next(0, 255), (byte)r.Next(0, 255));
            }
        }

        private bool flag4 = false;
        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            flag4 = !flag4;
            if (flag4)
            {
                Random r = new Random();

                var N = r.Next(0, 99);

                int[] indexes = new int[N];

                for (int i = 0; i < N; i++)
                {
                    indexes[i] = r.Next(0, 99);
                }
                n.DrawActiveRSCircles(indexes);
            }
            else
            {
                Random r = new Random();
                n.RSEmitActiveColor = Color.FromRgb((byte)r.Next(0, 255), (byte)r.Next(0, 255), (byte)r.Next(0, 255));
            }
        }

        private bool flag5 = false;
        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            flag5 = !flag5;
            if (flag5)
            {
                Random r = new Random();

                var N = r.Next(0, 99);

                int[] indexes = new int[N];

                for (int i = 0; i < N; i++)
                {
                    indexes[i] = r.Next(0, 99);
                }
                n.DrawNonActiveRSCircles(indexes);
            }
            else
            {
                Random r = new Random();
                n.RSEmitNonActiveColor = Color.FromRgb((byte)r.Next(0, 255), (byte)r.Next(0, 255), (byte)r.Next(0, 255));
            }
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            n.ClearRSCircles();
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            n.NumberOfBands = 10;
        }

        private void c0_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            //n.Background = new SolidColorBrush(Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255)));
            n.BackGround = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void c1_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            n.RectanglesColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void c2_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            n.PopUpColor = Color.FromArgb(255, (byte)random.Next(), (byte)random.Next(255), (byte)random.Next(255));
        }

        private void c3_Click(object sender, RoutedEventArgs e)
        {
            n.OverlayMHz = 25;
            n.BandWidthMHz = 125;
            n.RangeMinMHz = 10;
            n.NumberOfBands = 60;
        }

        private void Calc(double start, double bw, double overlap, double end)
        {
            int count = (int)Math.Ceiling((end - start) / (bw - overlap));
        }

    }
}
